# SimpleVR

Unity3D VR Framework.



Created by Daniel Castaño Estrella at Lince Works.

Contact: daniel.c.estrella@gmail.com

License in LICENSE file.



**IMPORTANT:** Only SteamVR dependency is included. Others should be downloaded and placed in Assets/Plugins folder.



## SimpleVR dependencies

- SteamVR
- SuperScience https://github.com/Unity-Technologies/SuperScience
  -GizmoModule
  -PhysicsTracker
- DOTween: Translate Grabbable on attach. https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676

## SimpleVR.Example dependencies

### InteractableFeedback

- Quick Outline [https://assetstore.unity.com/packages/tools/particles-effects/quick-outline-115488](https://assetstore.unity.com/packages/tools/particles-effects/quick-outline-115488)

## Recommended plugins

- VR Tunnelling Pro [https://github.com/sigtrapgames/VrTunnellingPro-Unity](https://github.com/sigtrapgames/VrTunnellingPro-Unity)